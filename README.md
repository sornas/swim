# Swim

A small build tool for the spade programming language. Builds spade code,
synthesises it (and at some point will simulate it), and uploads it for supported boards.

```
swim 0.1.0
The spade bulid tool

USAGE:
    swim [OPTIONS] <SUBCOMMAND>

OPTIONS:
    -h, --help       Print help information
    -q, --quiet      Tell the tools which support it to use quiet output
    -V, --version    Print version information

SUBCOMMANDS:
    build
    clean
    help      Print this message or the help of the given subcommand(s)
    pnr
    synth
    upload
```
