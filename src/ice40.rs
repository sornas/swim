use serde::Deserialize;

#[derive(Deserialize)]
pub enum Ice40Device {
    #[serde(rename = "iCE40LP384")]
    Ice40lp384,
    #[serde(rename = "iCE40LP1K")]
    Ice40lp1k,
    #[serde(rename = "iCE40LP4K")]
    Ice40lp4k,
    #[serde(rename = "iCE40LP8K")]
    Ice40lp8k,
    #[serde(rename = "iCE40HX1K")]
    Ice40hx1k,
    #[serde(rename = "iCE40HX4K")]
    Ice40hx4k,
    #[serde(rename = "iCE40HX8K")]
    Ice40hx8k,
    #[serde(rename = "iCE40UP3K")]
    Ice40up3k,
    #[serde(rename = "iCE40UP5K")]
    Ice40up5k,
    #[serde(rename = "iCE5LP1K")]
    Ice5lp1k,
    #[serde(rename = "iCE5LP2K")]
    Ice5lp2k,
    #[serde(rename = "iCE5LP4K")]
    Ice5lp4k,
}

impl Ice40Device {
    pub fn pnr_flag(&self) -> &str {
        match self {
            Ice40Device::Ice40lp384 => "--lp384",
            Ice40Device::Ice40lp1k => "--lp1k",
            Ice40Device::Ice40lp4k => "--lp4k",
            Ice40Device::Ice40lp8k => "--lp8k",
            Ice40Device::Ice40hx1k => "--hx1k",
            Ice40Device::Ice40hx4k => "--hx4k",
            Ice40Device::Ice40hx8k => "--hx8k",
            Ice40Device::Ice40up3k => "--up3k",
            Ice40Device::Ice40up5k => "--up5k",
            Ice40Device::Ice5lp1k => "--u1k",
            Ice40Device::Ice5lp2k => "--u2k",
            Ice40Device::Ice5lp4k => "--u4k",
        }
    }
}
