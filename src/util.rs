use std::{
    ffi::OsStr,
    path::{Path, PathBuf},
    time::SystemTime,
};

use color_eyre::{eyre::Context, Result};

/// Returns all files present in the specified directory
pub fn files_in_dir(dir: &Path, extension: &str) -> Result<Vec<PathBuf>> {
    let paths = std::fs::read_dir(dir)
        .with_context(|| format!("Failed to read files in {}", dir.to_string_lossy()))?;

    let files = paths
        .map(|path| Ok(path.context("Failed to read dir entry")?.path()))
        .collect::<Result<Vec<_>>>()
        .with_context(|| format!("While reading files in {}", dir.to_string_lossy()))?
        .into_iter()
        .filter(|path| path.is_file() && path.extension() == Some(&OsStr::new(extension)))
        .collect::<Vec<_>>();

    Ok(files)
}

pub fn modified_time(path: &Path) -> Result<SystemTime> {
    std::fs::metadata(path)
        .with_context(|| format!("Failed to get metadata of {:?}", path))?
        .modified()
        .with_context(|| format!("Failed to get modified time of {:?}", path))
}

pub fn needs_rebuild<'a>(
    target: &Path,
    sources: impl IntoIterator<Item = &'a PathBuf>,
) -> Result<bool> {
    if !target.exists() {
        return Ok(true);
    } else {
        let target_time = modified_time(target)?;

        Ok(sources
            .into_iter()
            .map(|source| modified_time(source))
            .collect::<Result<Vec<_>>>()?
            .into_iter()
            .any(|source_time| source_time > target_time))
    }
}
