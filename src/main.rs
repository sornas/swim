use std::{path::PathBuf, process::Command};

use clap::StructOpt;
use cmdline::Args;
use color_eyre::{
    eyre::{anyhow, Context},
    Result,
};
use itertools::Itertools;
use log::{error, info};

use config::Config;
use util::needs_rebuild;

use crate::util::files_in_dir;

mod cmdline;
mod config;
mod ice40;
mod util;

fn build_dir() -> PathBuf {
    PathBuf::from("build")
}

fn yosys_target_json() -> PathBuf {
    build_dir().join("hardware.json")
}

/// Build the spade compiler, returning a path to the resulting binary
fn build_spadec(config: &Config) -> Result<PathBuf> {
    info!("Building spadec");
    let current_dir = std::env::current_dir().context("Failed to get current working directory")?;

    let compiler_dir = current_dir.join(&config.compiler_dir);
    // Build the spade compiler
    let status = Command::new("cargo")
        .arg("build")
        .arg("--release")
        .current_dir(&compiler_dir)
        .status()
        .context("Failed to run cargo build")?;

    if !status.success() {
        Err(anyhow!("Cargo exited with status {:?}", status.code()))
    } else {
        info!("Built spadec");
        Ok(compiler_dir.join("target/release/spade"))
    }
}

/// Runs the spade compiler on the input source files to generate a spade file. Returns
/// the path to the generated file
///
/// Always calls cargo to rebuild the spade compiler
///
/// If the target is newer than the compiler and all the source files, nothing else is done
fn build_spade(config: &Config) -> Result<PathBuf> {
    if config.source_dirs.is_empty() {
        return Err(anyhow!(
            "Expected at least one directory in the source_dirs list"
        ))?;
    }

    let compiler = build_spadec(config).context("Failed to build spade compiler")?;

    let spade_files = config
        .source_dirs
        .iter()
        .map(|dir| util::files_in_dir(dir, "spade"))
        .collect::<Result<Vec<_>>>()?
        .into_iter()
        .flatten()
        .collect::<Vec<PathBuf>>();

    if spade_files.is_empty() {
        return Err(anyhow!(
            "Found no source files in any of the source directories"
        ));
    }

    let spade_target = build_dir().join("spade.sv");
    if needs_rebuild(
        &spade_target,
        spade_files
            .iter()
            .chain([&compiler].into_iter())
            .chain([&PathBuf::from("swim.toml")].into_iter()),
    )? {
        std::fs::create_dir_all(build_dir()).context("Failed to create build directory")?;

        info!("Building spade code");

        let status = Command::new(compiler)
            .args(spade_files)
            .arg("-o")
            .arg(&spade_target)
            .arg("--mir-output")
            .arg(build_dir().join("spade.spmir"))
            .status()
            .context("Failed to run spade compiler")?;

        if !status.success() {
            error!("Failed to build spade scode");
            Err(anyhow!("Failed to build spade code"))
        } else {
            info!("Built {}", spade_target.to_string_lossy());
            Ok(spade_target)
        }
    } else {
        info!("{} is up to date", spade_target.to_string_lossy());
        Ok(spade_target)
    }
}

/// Simulates the source files throwing an error if compilation fails, or
/// if the tests fail
fn simulate(config: &Config) -> Result<()> {
    let spade_target = build_spade(config)?;

    let global_extra_verilog = config.extra_verilog.clone().unwrap_or(vec![]);

    let test_sv = files_in_dir(&config.simulation.testbench_dir, "sv")?;
    let test_v = files_in_dir(&config.simulation.testbench_dir, "v")?;

    let sources = [&spade_target]
        .into_iter()
        .chain(global_extra_verilog.iter())
        .chain(test_sv.iter())
        .chain(test_v.iter())
        .collect::<Vec<_>>();

    let vvp = config.build_dir.join("tb.vvp");
    if needs_rebuild(&vvp, sources.clone())? {
        let status = Command::new("iverilog")
            .arg("-g2012")
            .arg("-o")
            .arg(&vvp)
            .args(sources)
            .status()
            .with_context(|| "Failed to run iverilog")?;

        if status.success() {
            info!("Built simulation executable");
        } else {
            return Err(anyhow!("Failed to compile simulation executable"));
        }
    }

    // We want to re-run tests even if no sources have changed
    let status = Command::new(vvp)
        .status()
        .with_context(|| "Failed to run test binary")?;

    if status.success() {
        info!("All tests passed");
        Ok(())
    } else {
        Err(anyhow!("At least one test failed"))
    }
}

/// Synthesise the source files. Returns the path of the generated json
fn synthesise(args: &Args, config: &Config) -> Result<PathBuf> {
    let spade_target = build_spade(config)?;

    let global_extra_verilog = config.extra_verilog.clone().unwrap_or(vec![]);
    let synth_extra_verilog = config.synthesis.extra_verilog.clone().unwrap_or(vec![]);
    let sources = [&spade_target]
        .into_iter()
        .chain(global_extra_verilog.iter())
        .chain(synth_extra_verilog.iter())
        .collect::<Vec<_>>();

    let target_json = yosys_target_json();
    if needs_rebuild(&target_json, sources.clone())? {
        let yosys_command_string = format!(
            r#"
            read_verilog -sv {sources};
            {synth_command} -top {top} -json {json};
            "#,
            sources = sources.iter().map(|s| s.to_string_lossy()).join(" "),
            synth_command = config.synthesis.command,
            top = config.synthesis.top,
            json = target_json.to_string_lossy()
        );

        let status = Command::new("yosys")
            .arg("-p")
            .arg(yosys_command_string)
            .args(if args.quiet { vec!["-q"] } else { vec![] })
            .status()
            .with_context(|| "Failed to run yosys")?;

        if status.success() {
            info!("Synthesised {}", target_json.to_string_lossy());
            Ok(target_json)
        } else {
            Err(anyhow!("Synthesis failure"))
        }
    } else {
        info!("{} is up to date", target_json.to_string_lossy());
        Ok(target_json)
    }
}

enum PnrResult {
    AscFile(PathBuf),
}

/// Run place and route on the source files.
fn run_pnr(args: &Args, config: &Config) -> Result<PnrResult> {
    let json = synthesise(args, config)?;

    match &config.pnr {
        config::Pnr::Ice40 {
            device,
            pcf,
            package,
            allow_unconstrained,
        } => {
            let target = build_dir().join("hardware.asc");

            if needs_rebuild(&target, [&json])? {
                let status = Command::new("nextpnr-ice40")
                    .arg(device.pnr_flag())
                    .arg("--package")
                    .arg(package)
                    .arg("--json")
                    .arg(json)
                    .arg("--pcf")
                    .arg(pcf)
                    .arg("--asc")
                    .arg(&target)
                    .args(if args.quiet { vec!["-q"] } else { vec![] })
                    .args(if *allow_unconstrained {
                        vec!["--pcf-allow-unconstrained"]
                    } else {
                        vec![]
                    })
                    .status()
                    .context("Failed to run nextpnr-ice40")?;

                if status.success() {
                    info!("Finished place and route");
                    Ok(PnrResult::AscFile(target))
                } else {
                    Err(anyhow!("Place and route failed"))
                }
            } else {
                info!("{} is up to date", target.to_string_lossy());
                Ok(PnrResult::AscFile(target))
            }
        }
    }
}

fn upload(args: &Args, config: &Config) -> Result<()> {
    match config.upload {
        // NOTE: iceprog and icesprog are similar enough but when a new tool is added,
        //       check what they have in common and possibly refactor.
        //
        //       https://en.wikipedia.org/wiki/Rule_of_three_(computer_programming)
        config::UploadTool::Icesprog | config::UploadTool::Iceprog => {
            let pnr_result = run_pnr(args, config)?;

            // NOTE: this if statement will get expanded when more tools are added
            #[allow(irrefutable_let_patterns)]
            if let PnrResult::AscFile(asc_file) = pnr_result {
                let bin_file = build_dir().join("hardware.bin");
                if needs_rebuild(&bin_file, [&asc_file].into_iter())? {
                    info!("Packing bin file");

                    let status = Command::new("icepack")
                        .arg(&asc_file)
                        .arg(&bin_file)
                        .status()
                        .context("Failed to run icepack")?;

                    if !status.success() {
                        return Err(anyhow!("Failed to pack bin file"));
                    }
                }

                info!("Uploading bin file");

                let status = Command::new(config.upload.binary())
                    .arg(bin_file)
                    .status()
                    .context("Failed to program device")?;

                if status.success() {
                    info!("Upload successful");
                    Ok(())
                } else {
                    Err(anyhow!("Failed to upload bin file"))
                }
            } else {
                Err(anyhow!(
                    "icesprog only supports asc files. Make sure pnr is set to ice40"
                ))
            }
        }
    }
}

fn main() -> Result<()> {
    env_logger::builder()
        .filter_level(log::LevelFilter::Info)
        .format_timestamp(None)
        .format_module_path(false)
        .format_target(false)
        .init();
    color_eyre::install()?;

    let args = cmdline::Args::parse();

    let config_str = std::fs::read_to_string("swim.toml").context("Failed to read swim.toml")?;

    let config =
        toml::from_str::<config::Config>(&config_str).context("Failed to parse swim.toml")?;

    match args.command {
        cmdline::Command::Build => {
            build_spade(&config)?;
        }
        cmdline::Command::Synth => {
            synthesise(&args, &config)?;
        }
        cmdline::Command::Pnr => {
            run_pnr(&args, &config)?;
        }
        cmdline::Command::Upload => {
            upload(&args, &config)?;
        }
        cmdline::Command::Simulate => {
            simulate(&config)?;
        }
        cmdline::Command::Clean => {
            if PathBuf::from("swim.toml").exists() {
                if build_dir().exists() {
                    info!("Removing build directory");
                    std::fs::remove_dir_all(build_dir())
                        .context("Failed to remove build directory")?;
                } else {
                    info!("Project is already clean");
                }
            } else {
                return Err(anyhow!("Did not find swim.toml"));
            }
        }
    }

    Ok(())
}
