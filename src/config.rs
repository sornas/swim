use std::path::PathBuf;

use serde::Deserialize;

use crate::ice40::Ice40Device;

fn default_build_dir() -> PathBuf {
    PathBuf::from("./build")
}

#[derive(Deserialize)]
pub struct Simulation {
    /// Directory containing all test benches
    pub testbench_dir: PathBuf,
}

#[derive(Deserialize)]
pub struct Synthesis {
    pub top: String,
    /// The yosys command to use for synthesis
    pub command: String,

    /// Extra verilog files only needed during the synthesis process
    pub extra_verilog: Option<Vec<PathBuf>>,
}

#[derive(Deserialize)]
#[serde(tag = "architecture")]
pub enum Pnr {
    #[serde(rename = "ice40")]
    Ice40 {
        device: Ice40Device,
        pcf: String,
        package: String,
        #[serde(default)]
        allow_unconstrained: bool,
    },
}

#[derive(Deserialize)]
#[serde(tag = "tool")]
pub enum UploadTool {
    #[serde(rename = "icesprog")]
    Icesprog,
    #[serde(rename = "iceprog")]
    Iceprog,
}

impl UploadTool {
    pub fn binary(&self) -> &'static str {
        match self {
            UploadTool::Icesprog => "icesprog",
            UploadTool::Iceprog => "iceprog",
        }
    }
}

#[derive(Deserialize)]
pub struct Config {
    pub compiler_dir: PathBuf,
    /// Directories containing spade source code
    pub source_dirs: Vec<PathBuf>,

    pub extra_verilog: Option<Vec<PathBuf>>,

    #[serde(default = "default_build_dir")]
    pub build_dir: PathBuf,

    pub simulation: Simulation,

    pub synthesis: Synthesis,

    pub pnr: Pnr,

    pub upload: UploadTool,
}
