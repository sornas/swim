use clap::Parser;

#[derive(Parser, Debug)]
pub enum Command {
    Build,
    Synth,
    Pnr,
    Upload,
    #[clap(
        visible_alias = "sim",
        visible_alias = "t",
        visible_alias = "test",
        visible_alias = "s"
    )]
    Simulate,
    Clean,
}

/// The spade bulid tool
#[derive(Parser, Debug)]
#[clap(version, about)]
pub struct Args {
    #[clap(subcommand)]
    pub command: Command,

    /// Tell the tools which support it to use quiet output
    #[clap(short = 'q', long)]
    pub quiet: bool,
}
